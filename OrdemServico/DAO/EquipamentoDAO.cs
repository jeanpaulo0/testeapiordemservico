﻿using MySql.Data.MySqlClient;
using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace OrdemServico.DAO
{
    public class EquipamentoDAO
    {
        public static DataTable ObterListaEquipamento()
        {
            MySqlConnection conn = null;
            MySqlDataAdapter da = null;

            string sql = @"select * from ma_equipamento where 1=1";

            try
            {
                DataTable dt = new DataTable();

                conn = DB.ConnectAmazonMySql();
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                conn.Open();
                da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                da.Dispose();

                return dt;
            }
            catch (Exception er)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}