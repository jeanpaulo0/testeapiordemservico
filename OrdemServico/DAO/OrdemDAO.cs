﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace OrdemServico.DAO
{
    public class OrdemDAO
    {

        public static DataTable ObterListaOS()
        {
            //string resultado        = null;
            MySqlConnection conn    = null;
            MySqlDataAdapter da     = null;

            //string sql = @"select * from ma_ordem where 1=1 order by ordem_id desc";
            string sql = @" select  o.ordem_id, o.titulo, o.descricao, 
                                    l.localizacao_id, l.descricao as local_desc, 
                                    e.equipamento_id, e.descricao as equip_desc
                            from    ma_ordem o
                            inner join ma_localizacao l using(localizacao_id)
                            inner join ma_equipamento e using(equipamento_id)
                            order by ordem_id desc";

            try
            {
                DataTable dt = new DataTable();

                conn = DB.ConnectAmazonMySql();
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                conn.Open();
                da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                da.Dispose();

                return dt;
                //resultado = Functions.ConverteDataTableParaString(dt);
                //return resultado;
            }
            catch (Exception er)
            {
                //Console.Out.Write(er.Message);
                //throw er;
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable ObterOS(int id)
        {
            //string resultado        = null;
            MySqlConnection conn = null;
            MySqlDataAdapter da = null;

            string sql = @"select * from ma_ordem where ordem_id = ?id";

            try
            {
                DataTable dt = new DataTable();

                conn = DB.ConnectAmazonMySql();
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("id", id);

                conn.Open();
                da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                da.Dispose();

                return dt;
            }
            catch (Exception er)
            {
                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static bool AbrirOrdem(JToken json)
        {
            MySqlConnection conn = null;
            
            dynamic resultado = JsonConvert.DeserializeObject(json.ToString());
            dynamic conteudo = resultado.jsonData;
            
            string titulo       = Convert.ToString(conteudo.titulo);
            string descricao    = Convert.ToString(conteudo.descricao);
            int localizacao     = Convert.ToInt32(conteudo.localizacao_id);
            int equipamento     = Convert.ToInt32(conteudo.equipamento_id);

            string sql = @"insert into ma_ordem(titulo, descricao, localizacao_id, equipamento_id)
                           values (@titulo, @descricao, @localizacao, @equipamento)";

            try
            {
                DataTable dt = new DataTable();
                conn = DB.ConnectAmazonMySql();

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@titulo", titulo);
                cmd.Parameters.AddWithValue("@descricao", descricao);
                cmd.Parameters.AddWithValue("@localizacao", localizacao);
                cmd.Parameters.AddWithValue("@equipamento", equipamento);

                conn.Open();
                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception Er)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }
    }
}