﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OrdemServico.Models
{
    public class OrdemModelView
    {

        public int Ordem_Id { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        
        [DisplayName("Localização")]
        public string Local_Desc { get; set; }

        public int Equipamento_Id { get; set; }
        


    }
}