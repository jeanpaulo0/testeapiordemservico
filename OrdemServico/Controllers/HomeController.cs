﻿using OrdemServico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OrdemServico.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult GetOrdens()
        {
            IEnumerable<OrdemModelView> ordens = null;

            using(var cliente = new HttpClient())
            {
                
                cliente.BaseAddress = new Uri("http://localhost:55910/api/");

                var responseTask = cliente.GetAsync("Ordem");
                responseTask.Wait();

                var result = responseTask.Result;

                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<OrdemModelView>>();
                    readTask.Wait();

                    ordens = readTask.Result;
                }
                else
                {
                    ordens = Enumerable.Empty<OrdemModelView>();
                    ModelState.AddModelError(string.Empty, "Erroooo!");
                }

            }

            return View(ordens);
        }
    }
}
