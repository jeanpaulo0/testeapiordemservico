﻿using Jose;
using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OrdemServico.Controllers.api
{
    [EnableCors(origins: "http://localhost:8080", headers: "*", methods: "*")]
    public class AuthController : ApiController
    {

        public HttpResponseMessage Get()
        {
            try
            {
                var payload = new Dictionary<string, object>()
                {
                    { "sub", "mr.x@contoso.com" },
                    { "exp", 1300819380 }
                };

                string token = Jose.JWT.Encode(payload, null, JwsAlgorithm.none);

                var resposta = this.Request.CreateResponse(HttpStatusCode.OK);
                resposta.Content = new StringContent(payload.ToString(), Encoding.UTF8, "application/json");

                return resposta;
            }
            catch (Exception Er)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

    }
}
