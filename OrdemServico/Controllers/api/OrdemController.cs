﻿using Newtonsoft.Json.Linq;
using OrdemServico.DAO;
using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OrdemServico.Controllers.api
{
    [EnableCors(origins: "http://localhost:8080", headers: "*", methods: "*")]
    public class OrdemController : ApiController
    {
        // GET: api/Ordem/Listar
        [HttpGet]
        public HttpResponseMessage Listar()
        {
            try
            {
                DataTable dt = OrdemDAO.ObterListaOS();
                
                HttpResponseMessage resposta = TratamentoApi.ObterResposta(this.Request, dt);

                return resposta;
            }
            catch (Exception Er)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // GET: api/Ordem/5
        [HttpGet]
        public HttpResponseMessage Obter(int id)
        {
            try
            {
                DataTable dt = OrdemDAO.ObterOS(id);

                HttpResponseMessage resposta = TratamentoApi.ObterResposta(this.Request, dt);

                return resposta;
            }
            catch (Exception Er)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // POST: api/Ordem
        public HttpResponseMessage Post([FromBody] JToken json)
        {
            try
            {
                if (OrdemDAO.AbrirOrdem(json))
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (Exception Er)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // PUT: api/Ordem/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Ordem/5
        public void Delete(int id)
        {
        }
    }
}
