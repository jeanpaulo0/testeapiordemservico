﻿using OrdemServico.DAO;
using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OrdemServico.Controllers.api
{
    [EnableCors(origins: "http://localhost:8080", headers: "*", methods: "*")]
    public class EquipamentoController : ApiController
    {

        [HttpGet]
        public HttpResponseMessage Listar()
        {
            try
            {
                DataTable dt = EquipamentoDAO.ObterListaEquipamento();

                HttpResponseMessage resposta = TratamentoApi.ObterResposta(this.Request, dt);

                return resposta;
            }
            catch (Exception Er)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

    }
}
