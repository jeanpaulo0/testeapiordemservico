﻿using OrdemServico.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrdemServico.Controllers
{
    public class TestesController : ApiController
    {

        public string Get()
        {
            var x = DB.TestaConexao();
            return x.ToString();
        }

    }
}
