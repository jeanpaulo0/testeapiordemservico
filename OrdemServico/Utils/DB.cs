﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace OrdemServico.Utils
{
    public class DB
    {

        public static MySqlConnection ConnectAmazonMySql()
        {
            return new MySqlConnection( ConfigurationManager.ConnectionStrings["MySqlDB"].ConnectionString );
        }

        public static bool TestaConexao()
        {
            MySqlConnection conn = ConnectAmazonMySql();

            try
            {
                conn.Open();
                conn.Close();
                return true;
            }
            catch (Exception er)
            {
                return false;
            }

        }

    }
}