﻿using Newtonsoft.Json;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;

namespace OrdemServico.Utils
{
    public class TratamentoApi
    {

        public static string ConverteDataTableParaString(DataTable tabela)
        {
            try
            {
                // Cria vazia para retorno
                string resultado = string.Empty;

                if (tabela.Rows.Count > 0)
                {
                    // Necessita importar Newtonsoft.Json
                    resultado = JsonConvert.SerializeObject(tabela);
                } 
                else
                {
                    resultado = null;
                }

                return resultado;
            }
            catch
            {
                return null;
            }
        }

        public static HttpResponseMessage ObterResposta(HttpRequestMessage request, DataTable dt)
        {
            string resultado = ConverteDataTableParaString(dt);

            if (!string.IsNullOrEmpty(resultado))
            {
                var resposta = request.CreateResponse(HttpStatusCode.OK);
                resposta.Content = new StringContent(resultado, Encoding.UTF8, "application/json");
                return resposta;
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.NoContent);
            }
        }
    }
}